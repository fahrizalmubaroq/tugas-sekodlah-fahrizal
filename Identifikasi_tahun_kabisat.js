var Year = function(number) {
    this.number = number;
  };
  
  Year.prototype.isLeap = function() {
    return this.isDivisibleBy(4) && !this.isDivisibleBy(100) || this.isDivisibleBy(400);
  }
  
  Year.prototype.isDivisibleBy = function(divisor) {
    return this.number % divisor === 0;
  }
  
  const commonYears = [2001, 2002, 2003, 2005, 2006, 2007, 2009, 2010, 2011, 2013, 2014, 2015, 2017, 2018, 2019, 2021, 2022, 2023]
  const leapYears = [2000, 2004, 2008, 2012, 2016, 2020, 2024]
  
  commonYears.forEach(y => console.log(y, new Year(y).isLeap()))
  leapYears.forEach(y => console.log(y, new Year(y).isLeap()))